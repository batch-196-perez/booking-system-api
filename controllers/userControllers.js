/*
	Naming convention for controllers is that it should be named after the model/documents it is concerned with

	Controllers are functions which contain the actual business logic of our API and is triggered by a route

	MVC - models views and controller
*/

//To create a controller, we first add it into our module.exports
// So that we can import the controllers from our module

// import the user model in the controllers instead because this where are now going to use it
const User = require("../models/User");

//Import Course

const Course = require("../models/Course");
//Import bcrypt
//bcrypt is a package which allows us to hash our passwords to add a layer of security for our's 
const bcrypt = require("bcrypt");

//import auth.js module to use createAccessToken and its subsequent methods 
const auth = require("../auth")

module.exports.registerUser = (req,res)=> {

	//console.log(req.body) //check the input passed via the client
	// using bcrypt , hide the users password underneath the a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash

	//bcrypt 
	const hashedPw = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPw);

	let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email:req.body.email,
		password:hashedPw,
		mobileNo: req.body.mobileNo
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//getUserDetails should only allow the logged-in user his OWN details
module.exports.getUserDetails = (req,res)=>{
	//console.log(req.user)//contains the details of the logged in user
	//find() will return an array of documents which matches the criteria
	// It will return an array even if it only found 1 document.
	//User.findOne({_id:req.body.id})

	//findOne() will return a single document that matchd our criteria
	//User.findOne({_id:req.body.id})

	//findById() is a mongoose method that will allow us to find a document strictly by its id

	//This will allow us to ensure that the logged in user or the user that passed the token will be able to get his own details and only his own
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.logInUser = (req,res) => {

	console.log(req.body);
	/*
		Steps for logging in our user:

		1. Find the user by its email
		2. If we found the user , we will check his password input and the hashed password in our db matches
		3. If we dont find a user, we will send a message to the client.
		4. If upon checking the found user's password is the same our input password, we will generate a "key" for the user to have authorization to access certein features in our apps

	*/
	//mongodb db.users.findone({})
	User.findOne({email:req.body.email})
	.then(foundUser => {

		//foundUser is the parameter that contains the result of findOne
		//findOne() returns null if its not able to match document
		if(foundUser === null){
			return res.send({message: "No User Found."})//CLient will receive this object if no user is found
		} else {
			//console.log(foundUser)
			//if we find a user, foundUser will contain the document that matched the input email.

			//Check if input password from req.body matches the hashed password in our foundUser document

			/*
				bcrypt.compareSync(<inputString>, <hashedString>)
			
				If the inputString and hashedString matches and are the same, the compareSync method will return tru. else, it will return false
			*/
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			//console.log(isPasswordCorrect)

			//if the password is correct, we will create a token for our user, else, we will send a message:
			if(isPasswordCorrect){
				/*
					To be able to create a "key" or token that allows/authorizes our logged in user around our application, we have to create our own module called auth.jas

					This module will create a encoded string which contains our user's details

					THis encoded string is what we call a JSONWebToken or JWT

					This JWT can only properly unwrapped or decoded with our own secret word/string


				*/



				//console.log("We will create a token for the user if the password is correct")

				//auth.createAccessToken receives our foundUser document as an argument, gets only necessary details, wraps those details in JWT and our secret, then finally returns our JWT. This JWT will be sent to our client
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			}else{
				return res.send({message: "Incorrect Password"});
			}
		}
	})


}

//can be accessed by anyone email
module.exports.checkUserEmail = (req,res) =>{

	User.findOne({email:req.body.email})
	.then(result=>{

		if(result === null){

			return res.send(false)
		}else {
			return res.send(true)
		}
	})
	.catch(error => res.send(error))
}


module.exports.enroll = async(req,res) => {

	//check the id of the user?
		//console.log(req.user.id);
	//check the id of the course we want to enroll?
	//console.log(req.body.courseId);

	//validate the user if they are admin or not
	//If the user is an admin, send message to client and end the response
	//else, we will continue


	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	} else {

		/*
			Enrollment will come in 3 steps

			First, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in the enrollments array.
			Second, find the course where we are enrolling and update its enrollees subdocument array, we will push the userId in the enrollees array

			Since we will access 2 collections in one action, we will have to wait for the completion of the action instead of letting Javascript continue line per line.

			async and await - async keyword is added to a function to make our asynchronous. Which means that instead of JS regular behavior of running each code line by line we will be able to wait for the result of a function

			to be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get result before processing

		*/

		//return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array.

		let isUserUpdated = await User.findById(req.user.id).then(user =>{

			//console.log(user);

			//add the courseId in an object and push that object into our users enrollment
			//because we have to follow the schema of the enrollments subdocument array:

			let newEnrollment = {
				courseId: req.body.courseId
			}
			//acces the enrollments array from our user and push the new enrollment subdocument into the enrollments array
			user.enrollment.push(newEnrollment)


			//we must save the user document and return the value of saving our documents.
			//then return true if we push the subdocument successfull
			//catch and return error message if otherwise


			return user.save().then(user=>true).catch(err=>err.message)

		})

		//if user was able to enroll, properly, 'isUserupdated contains true'
		//Else, isUserUpdated will contain an error message
		//console.log(isUserUpdated);

		//Add an if statement and stop the process IF isUserUpdated DOES not contain true

		if(isUserUpdated !== true){
			return res.send({message: isUserUpdated});
		}

		//find the course where we will enroll or add the user as an enrolle and return true if we are able to push the user in to the enrollees array properly or send the error message instead


		let isCourseUpdated = await Course.findById(req.body.courseId).then(course=> {

			//console.log(course);//contain the found course or the course we want to enroll

			//create a object to pushed into the subdocument array, enrolles
			//we have to follow the schema of our subdocument array
			let enrollee = {
				userId : req.user.id
			}

			//push the enrolle into the enrollees subdocument array of the course:

			course.enrollees.push(enrollee);

			return course.save().then(course=> true).catch(err=> err.message);
		})

		//console.log(isUserUpdated);
		// If isCourseUpdated does not contain true, send the error message to the client and stop the process
		if(isCourseUpdated !== true){
			return res.send({message: isCourseUpdated})
		}
		
		if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Thank you for enrolling!"})
		}

	}

	//Ensure that we are able to both update the user and course document to add our enrollment and enrolle resp


}