const express = require("express");
/*A mongoose is on DOM Library to let our express API manipulate a mongodb database*/
const mongoose = require("mongoose");
const app = express();
const port = 4000;


/*
	Mongoose Connection

	mongoose.connect() is a method to connect our api with our mongodb database via the use of mongoose. It has 2 arguments. First, is the connection string to connect our api to our mongodb. Second. is an object user to add information betweem moongose and mongodb

	replace/change <password> in the connection string to your db user password

	just before the ? in the connection string add the database name.

*/

mongoose.connect("mongodb+srv://admin:admin123@cluster0.yrqmi.mongodb.net/BookingAPI?retryWrites=true&w=majority",
	{

		useNewUrlParser:true,
		useUnifiedTopology:true
	});
// We will create notification if the connection to the db is a success or failed
let db = mongoose.connection;
//this is to show notification of an internal server error from mongodb
db.on('error',console.error.bind(console, "MongoDB COnnection Error"));

//if the connections is open and successful we will output a message in the terminal

db.on('open',()=>console.log("Connected to MongDB"));


// express.json() to be able to handle the request body and parse it into

app.use(express.json());

// import our routes and use it as middleware
//which means, that we will be able to group together our routes
const courseRoutes = require('./routes/courseRoutes');
//use our routes and group then together under '/course'
// our endpoint are now preface with /courses
app.use('/courses',courseRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);


app.listen(port,() => console.log(`Express API running at port 4000`))