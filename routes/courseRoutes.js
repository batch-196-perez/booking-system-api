/*
	To be able to create routes from another file to be used



	the Router() method will us to container our routes
*/


const express = require("express");
const router = express.Router();



//All route to courses now has an endpoint prefaced with /courses 
//endpoint - /courses/courses

const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//gets All courses documents wether it is active or inactive
router.get('/',verify,verifyAdmin,courseControllers.getAllCourses);

//endpoint /courses/
// Only logged in user is able to use addCourse
	router.post('/',verify,verifyAdmin,courseControllers.addCourse);


// Get all active courses - (regular non logged in)

router.get('/activeCourses', courseControllers.getActiveCourses);

//endpoint/:route params
//route param
router.get('/getSingleCourse/:courseId', courseControllers.getSingleCourse);


//update single course
router.put('/updateCourse/:courseId',verify,verifyAdmin, courseControllers.updateCourse);
module.exports = router;

//Archive a single course
//Pass the id for the course we want to update via route params
//we will directly update course as inactive

router.delete('/archiveCourse/:courseId',verify,verifyAdmin,courseControllers.archiveCourse);



