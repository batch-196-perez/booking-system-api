const express = require("express");
const router = express.Router();



//In fact, route should just contain the endpoints and it should only trigger the function but it should not be where we define the logic of our function

// The business logic of our API should be in controllers

//import our user controllers
const userControllers = require("../controllers/userControllers");

//console.log(userControllers);


//import auth to be able to have access and use the verify methods to act as middleware for our routes
// Middleware add in the route such as verify() will have access to the req,res

const auth = require("../auth");


//destructure auth to get only your methods and save it in variables
//middleware add in the route such as verify() will have access
const {verify} = auth;





/*
	Updated Route Syntax

	router.method("/endpoint", handlerFunction)

*/

//register

router.post("/",userControllers.registerUser);

// POST method route to get user details by id
//verify() is used as a middleware which means our requet will get through verify first before our controller
//verify() will not only check the validity of the token but also decoded data of the token in the request object as req.user
router.get("/details",verify,userControllers.getUserDetails);

//Route for User Authentication
router.post('/login',userControllers.logInUser);

router.post('/checkEmail',userControllers.checkUserEmail);

//User Enrollment
// CourseId will come from the req.body
//userId will come from req.user

router.post('/enroll',verify,userControllers.enroll);


module.exports = router;


